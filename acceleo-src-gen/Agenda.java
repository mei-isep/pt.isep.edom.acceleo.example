class Agenda implements Topic {

	// The Requirements Model Title: Agenda
	// The The Number of RequirementsGroups of the Requirements Model: 2

	public void execute() {

		boolean exit=false;

		java.util.Scanner in = new java.util.Scanner(System.in);

		while (!exit) {

			System.out.println("Map Agenda");

			System.out.println("My root topics are:");

			System.out.println("1-Contacts");
			System.out.println("2-Meetings");
			System.out.println("0-Exit");

			System.out.println(">>Please enter you option");

			// Read an integer from the input
			int num = in.nextInt();

			switch (num) {
				case 1:
					{
						Contacts o=FFactory.getInstance().createContacts();
						o.execute();
					}
					break;
				case 2:
					{
						Meetings o=FFactory.getInstance().createMeetings();
						o.execute();
					}
					break;
				case 0:
					exit=true;
			}
		}
	}
}
