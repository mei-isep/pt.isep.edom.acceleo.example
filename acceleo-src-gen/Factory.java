
public interface Factory {

	public Agenda createAgenda();
	public SubContacts createSubContacts();
	public Contacts createContacts();
	public Meetings createMeetings();
}
