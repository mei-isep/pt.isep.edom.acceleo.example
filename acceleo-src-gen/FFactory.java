public class FFactory {

	private static Factory instance=null; 
	
    /**
     * get instance of Factory
     *
     * @generated NOT
     */
	public static Factory getInstance() {
		if (instance==null) {
			instance=new FactoryImpl2();
		}
		return instance;
	} 	
}
