
public class FactoryImpl implements Factory {

	public Agenda createAgenda() {
		return new Agenda();
	}

	public SubContacts createSubContacts() {
		return new SubContacts();
	}
	public Contacts createContacts() {
		return new Contacts();
	}
	public Meetings createMeetings() {
		return new Meetings();
	}
}
