
class Contacts implements Topic {

	public void execute() {

		boolean exit=false;

		java.util.Scanner in = new java.util.Scanner(System.in);

		while (!exit) {

			System.out.println("Topic Contacts");

			System.out.println("My sub topics are:");

			System.out.println("1-SubContacts");
			System.out.println("0-Exit");

			System.out.println(">>Please enter you option");

			// Read an integer from the input
			int num = in.nextInt();

			switch (num) {
				case 1:
					{
						SubContacts o=FFactory.getInstance().createSubContacts();
						o.execute();
					}
					break;
				case 0:
					exit=true;
			}
		}
	}
}

