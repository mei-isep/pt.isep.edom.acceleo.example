package pt.isep.edom.acceleo.example.main;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import pt.isep.edom.mindmap.mindmap.Map;
import pt.isep.edom.mindmap.mindmap.MindmapFactory;
import pt.isep.edom.mindmap.mindmap.MindmapPackage;
import pt.isep.edom.requirements.Model;
import pt.isep.edom.requirements.RequirementsFactory;
import pt.isep.edom.requirements.RequirementsPackage;

public class ReqModelQuery {

	private static Model myModel = null;
	
	private static String modelPath = null;
	
	public static void setModelPath(String path) {
		modelPath=path;
	}

	public ReqModelQuery() {
	}

	private Model getModel() {
		if (myModel != null) {
			return myModel;
		} else {
			Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("requirements",
					new XMIResourceFactoryImpl());

			// Initialize the model
			RequirementsPackage.eINSTANCE.eClass();

			// Retrieve the default factory singleton
			RequirementsFactory factory = RequirementsFactory.eINSTANCE;

			// Obtain a new resource set
			ResourceSet resSet = new ResourceSetImpl();

			Resource resource = resSet.getResource(URI.createURI(modelPath), true);

			try {
				resource.load(Collections.EMPTY_MAP);

				EObject root = resource.getContents().get(0);
				myModel = (Model) root;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			}
			return myModel;
		}
	}

	public String getReqModelTitle(Map map) {
		Model aModel=getModel();
		return aModel.getTitle();
	}

	public Model getReqModel(Map map) {
		Model aModel=getModel();
		return aModel;
	}
}
